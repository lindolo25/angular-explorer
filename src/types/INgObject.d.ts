import { Uri } from "vscode";
import { INgFileCollection } from "./INgFileCollection";
import { NgObjectType } from "../obj/ngObjectType";

export declare interface INgObject extends INgFileCollection {
    readonly fileName: string;
    readonly className: string;
    readonly type: NgObjectType;
    readonly dir: Uri;
    readonly scriptUri: Uri;
    readonly decorator: string;
}