import { Uri } from "vscode";
import { NgObjectType } from "../obj/ngObjectType";

export declare interface INgObjectTreeItem {
    readonly type: NgObjectType;
    readonly templateUri?: Uri;
    readonly styleUris?: Uri[];
    readonly specUri?: Uri; 
    readonly hasChildren: boolean;
}