export declare interface INgProjectTreeItem {
    readonly project: string;
    readonly projectType: "application" | "library";
    readonly sourceRoot: string;
}