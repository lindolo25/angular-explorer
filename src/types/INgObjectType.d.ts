import { NgFileType } from "../obj/ngFileType"

export declare interface INgObjectType {
    readonly identifier: string
    readonly label: string
    readonly possibleFileTypes: NgFileType[]
    readonly priority: number
    has(file: NgFileType): boolean
}