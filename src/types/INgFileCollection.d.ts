import { Uri } from 'vscode';

export declare interface INgFileCollection {
    readonly templateUri?: Uri;
    readonly styleUris?: Uri[];
    readonly specUri?: Uri;
}

export declare interface INgFileData extends INgFileCollection {
    readonly decorator: string;
    readonly decoratorContent: string;
    readonly className: string;
}