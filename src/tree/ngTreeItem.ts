import { TreeItem, TreeItemCollapsibleState } from 'vscode';

export class NgTreeItem extends TreeItem {

  constructor(
        public readonly project: string,
        public label: string, 
        public collapsibleState: TreeItemCollapsibleState
  ) {
    super(label, collapsibleState);
  }
}