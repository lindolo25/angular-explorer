import { TreeDataProvider, TreeItemCollapsibleState, workspace, TreeItem, ThemeIcon, Uri } from 'vscode';

import { NgTreeItem } from './ngTreeItem';
import { NgObjectTreeItem } from './ngObjectTreeItem';
import { NgTypeTreeItem } from './ngTypeTreeItem';
import { NgDataResolver } from './angularDataResolver';
import { NgObjectType } from '../obj/ngObjectType';
import { NgProjectTreeItem } from './ngProjectTreeItem';


export class AngularDataProvider extends NgDataResolver implements TreeDataProvider<NgTreeItem> {

  public readonly onDidChangeTreeData = this._onDidChangeTreeData.event;

  constructor() {
    super();
    this.refresh();
    this._initEvents();
  }

  private _initEvents() {
    workspace.onDidCreateFiles(this.refresh.bind(this));
    workspace.onDidDeleteFiles(this.refresh.bind(this));
    workspace.onDidRenameFiles(this.refresh.bind(this));
    workspace.onDidChangeConfiguration((e) => {
      if (e.affectsConfiguration("angularExplorer")) this.refresh();
    });
  }

  public refresh() {
    this._loadProjects();
  }

  public getTreeItem(treeItem: NgTreeItem): TreeItem | Promise<TreeItem> {
    if (treeItem instanceof NgObjectTreeItem && treeItem.hasChildren) {
      treeItem.collapsibleState = TreeItemCollapsibleState.Collapsed;
      treeItem.iconPath = ThemeIcon.File;
    }
    return treeItem;
  }

  public getChildren(item?: TreeItem): Promise<NgTreeItem[]> {
    if (!item) return Promise.resolve(this._ngProjects);
    else if (item instanceof NgProjectTreeItem) return this._getRootItems(item);
    else if (item instanceof NgTypeTreeItem && item.type) return this._getTypeChildren(item.project, item.type);
    else if (item instanceof NgObjectTreeItem) return this._getItemChildren(item.project, item.label, item.templateUri, item.styleUris, item.specUri);
    else return Promise.resolve([]);
  }

  private _getRootItems(project: NgProjectTreeItem): Promise<NgTreeItem[]> {
    const items = this._ngProjectTypeTreeItems.get(project.project);
    if (items) return Promise.resolve(items);
    else return this._loadTreeItems(project);
  }

  private _getTypeChildren(project: string, type: NgObjectType): Promise<NgTreeItem[]> {
    const items = this._ngProjectTreeItems.get(project);
    if (!items) return Promise.resolve([]);
    const result = items.filter(item => item.type == type)
      .sort((a, b) => a.label > b.label ? 1 : a.label < b.label ? -1 : 0);
    return Promise.resolve(result);
  }

  private _getItemChildren(project: string, label: string, template?: Uri, styles?: Uri[], spec?: Uri): Promise<NgTreeItem[]> {
    const files = [];
    if (template) files.push(this._createFileTreeItem(project, 'template', template, label));
    if (styles) styles.forEach(style => files.push(this._createFileTreeItem(project, 'style', style, label)));
    if (spec) files.push(this._createFileTreeItem(project, 'spec', spec, label));
    return Promise.resolve(files);
  }

  private _createFileTreeItem(project: string, type: string, uri: Uri, label: string): NgTreeItem {
    const file = new NgTreeItem(project, uri?.toString() as string, TreeItemCollapsibleState.None);
    file.resourceUri = uri;
    const ext = uri?.toString().split('.').reverse()[0];
    file.label = `${label}.${type}.${ext}`;
    file.command = {
      command: 'angularExplorer.openNgObject',
      title: '',
      arguments: [file]
    };
    return file;
  }
}