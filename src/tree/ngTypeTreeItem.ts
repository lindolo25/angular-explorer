import { TreeItemCollapsibleState } from 'vscode';
import { NgTreeItem } from './ngTreeItem';
import { NgObjectType } from '../obj/ngObjectType';


export class NgTypeTreeItem extends NgTreeItem {

  private _count = 1;

  constructor(
    _project: string,
        public readonly type: NgObjectType
  ) {
    super(_project, type.label, TreeItemCollapsibleState.Collapsed);
    this._setLabelInfo(this._count);
  }

  public get count(): number {
    return this._count;
  }

  public increaseObjectCount(): void {
    this._count++;
    this._setLabelInfo(this._count);
  }

  private _setLabelInfo(count: number): void {
    this.label = `${this.type.label} (${count})`;
  }
}