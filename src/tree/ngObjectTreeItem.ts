import { TreeItemCollapsibleState, Uri } from 'vscode';
import { NgTreeItem } from './ngTreeItem';
import { NgObject } from '../obj/ngObject';
import { INgObjectTreeItem } from '../types/INgObjectTreeItem';
import { NgObjectType } from '../obj/ngObjectType';


export class NgObjectTreeItem extends NgTreeItem implements INgObjectTreeItem {

  constructor(
    _project: string,
        private _ngObject: NgObject
  ) {
    super(_project, _ngObject.className, TreeItemCollapsibleState.None);
    this.resourceUri = this._ngObject.scriptUri;
    this.command = {
      command: 'angularExplorer.openNgObject',
      title: '',
      arguments: [this]
    };
    this.contextValue = `ngObject.${this._ngObject.type.identifier}`;
    this.tooltip = this._ngObject.decorator;
  }

  public get type(): NgObjectType {
    return this._ngObject.type;
  }

  public get templateUri(): Uri | undefined {
    return this._ngObject.templateUri;
  };

  public get styleUris(): Uri[] | undefined {
    return this._ngObject.styleUris;
  };

  public get specUri(): Uri | undefined {
    return this._ngObject.specUri;
  };

  public get hasChildren(): boolean {
    return !!(this._ngObject.templateUri || this._ngObject.specUri || (this._ngObject.styleUris && this._ngObject.styleUris.length > 0));
  }
}