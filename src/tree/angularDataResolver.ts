import { EventEmitter, Uri, workspace } from "vscode";
import { NgObjectTreeItem } from "./ngObjectTreeItem";
import { NgTypeTreeItem } from "./ngTypeTreeItem";
import { NgTreeItem } from "./ngTreeItem";
import { Util } from "../util";
import { NgObject } from "../obj/ngObject";
import { NgProjectTreeItem } from "./ngProjectTreeItem";


export abstract class NgDataResolver {

  protected _ngProjects: NgProjectTreeItem[] = [];
  protected _ngProjectTypeTreeItems = new Map<string, NgTypeTreeItem[] | null>();
  protected _ngProjectTreeItems = new Map<string, NgObjectTreeItem[] | null>();

  protected _onDidChangeTreeData = new EventEmitter<NgTreeItem | undefined>();

  constructor() { }

  protected _loadTreeItems(project: NgProjectTreeItem): Promise<NgTreeItem[]> {
    return Util.findFiles('/*.ts', project.sourceRoot)
      .then(uris => {
        if (!uris.length) return [];
        const promises = uris.map(uri => this._getNgObjectTreeItem(project.project, uri));
        return Promise.all(promises);
      })
      .then(items => items.forEach(item => this._buildItemState(item, project.project)))
      .then(() => this._ngProjectTypeTreeItems.get(project.project) || [])
      .catch(error => {
        console.log(error);
        return [];
      });
  }

  protected _loadProjects() {
    this._ngProjects = [];
    this._ngProjectTypeTreeItems.clear();
    this._ngProjectTreeItems.clear();
    Util.findNgConfigFile()
      .then(uri => this._openNgConfig(uri))
      .then(result => {
        if (!result) return;
        Object.keys(result.projects).forEach(key => {
          this._ngProjects.push(NgProjectTreeItem.fromProject(key, result.projects[key]));
          this._ngProjectTypeTreeItems.set(key, null);
          this._ngProjectTreeItems.set(key, null);
        });                
      })
      .catch(error => console.log(error))
      .finally(() => this._onDidChangeTreeData.fire(undefined));
  }

  private _openNgConfig(uri: Uri | null): Promise<{ [s: string]: any }> | null {
    if (!uri) return null;
    return Util.fromThenable(workspace.openTextDocument(uri))
      .then(document => JSON.parse(document.getText()));
  }

  private _getNgObjectTreeItem(project: string, scriptUri: Uri): Promise<NgObjectTreeItem | null> {
    return NgObject.fromScriptUri(scriptUri)
      .then(ngObject => new NgObjectTreeItem(project, ngObject))
      .catch(() => null);
  }

  private _buildItemState(item: NgObjectTreeItem | null, project: string) {
    if (!item) return;
    const items = this._ngProjectTreeItems.get(project) || [];
    items.push(item);
    this._ngProjectTreeItems.set(project, items);
    const types = this._ngProjectTypeTreeItems.get(project) || [];
    const type = types.find(type => type.type == item.type);
    if (type) type.increaseObjectCount();
    else types.push(new NgTypeTreeItem(project, item.type));
    this._ngProjectTypeTreeItems.set(project, types);
  }
}