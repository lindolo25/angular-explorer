import { TreeItemCollapsibleState } from "vscode";
import { INgProjectTreeItem } from "../types/INgProjectTreeItem";
import { NgTreeItem } from "./ngTreeItem";

export class NgProjectTreeItem extends NgTreeItem implements INgProjectTreeItem {

  constructor(
    _project: string,
        private _projectType: INgProjectTreeItem['projectType'],
        private _sourceRoot: string
  ) {
    super(_project, _project, TreeItemCollapsibleState.Collapsed);
  }

  public static fromProject(name: string, project: { [k: string]: any }) {
    return new NgProjectTreeItem(name, project.projectType, project.sourceRoot);
  }

  public get projectType(): INgProjectTreeItem['projectType'] {
    return this._projectType;
  }

  public get sourceRoot(): string {
    return this._sourceRoot;
  }
}