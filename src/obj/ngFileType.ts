export enum NgFileType {
    script,
    template,
    style,
    spec
}