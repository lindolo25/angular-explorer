import { Uri } from 'vscode';
import { NgObjectType } from './ngObjectType';
import { NgFileCollection } from './ngFileCollection';
import { INgObjectType } from '../types/INgObjectType';
import { INgObject } from '../types/INgObject';

export class NgObject extends NgFileCollection implements INgObject  {

  private constructor(
        protected _fileName: string,
        protected _className: string,
        protected _type: INgObjectType,
        protected _scriptDir: Uri,
        protected _scriptUri: Uri,
        protected _decorator: string = "",
        protected _decoratorContent: string,
        protected _templateUri?: Uri,
        protected _styleUris?: Uri[],
        protected _specUri?: Uri,
  ) {
    super();
  }

  //#region [rgba(255,255,0,0.07)] Public properties
    
  public get fileName(): string { return this._fileName; }

  public get className(): string { return this._className; }

  public get type(): NgObjectType { return this._type; }

  public get dir(): Uri { return this._scriptDir; }

  public get scriptUri(): Uri { return this._scriptUri; }

  public get decorator(): string { return this._decorator || ""; }

  //#endregion

  public static fromScriptUri(script: Uri): Promise<NgObject> {
    const uriParts = script.toString().split('/');
    const scriptFileName = uriParts[uriParts.length - 1];
    const scriptFileNameParts = scriptFileName.split('.');
    const name = scriptFileNameParts.slice(0, scriptFileNameParts.length - 1).join('.');
    const scriptDir = Uri.parse(uriParts.slice(0, uriParts.length - 1).join('/'));
    return this._readFiles(script, name, scriptDir)
      .then(files => new NgObject(
        scriptFileName,
        files.className,
                NgObjectType.getByIdentifier(files.decorator) as NgObjectType,
                scriptDir,
                script,
                files.decorator,
                files.decoratorContent,
                files.templateUri,
                files.styleUris,
                files.specUri
      ));
  }
}