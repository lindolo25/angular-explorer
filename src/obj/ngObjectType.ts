import { INgObjectType } from '../types/INgObjectType';
import { NgFileType } from './ngFileType';

export class NgObjectType implements INgObjectType {

  public static readonly types = [
    new NgObjectType('Component', 'Components', [NgFileType.script, NgFileType.template, NgFileType.style, NgFileType.spec], 10),
    new NgObjectType('Injectable', 'Services', [NgFileType.script, NgFileType.spec], 8),
    new NgObjectType('Directive', 'Directives', [NgFileType.script, NgFileType.spec], 5),
    new NgObjectType('Pipe', 'Pipes', [NgFileType.script, NgFileType.spec], 4),
    new NgObjectType('Guard', 'Guards', [NgFileType.script, NgFileType.spec], 3),
    new NgObjectType('NgModule', 'Modules', [NgFileType.script], 1),
  ];

  public static getByIdentifier(identifier: string): NgObjectType | undefined {
    return this.types.find(type => type.identifier === identifier);
  }

  constructor(
        public readonly identifier: string,
        public readonly label: string,
        public readonly possibleFileTypes: NgFileType[],
        public readonly priority = 0
  ) {}

  has(fileType: NgFileType): boolean {
    return this.possibleFileTypes.indexOf(fileType) > -1;
  }

}

