import { Uri, workspace } from 'vscode';
import { Util } from '../util';
import { NgObjectType } from './ngObjectType';
import { NgFileType } from './ngFileType';
import { INgFileCollection, INgFileData } from '../types/INgFileCollection';


export abstract class NgFileCollection implements INgFileCollection {

    protected abstract _templateUri?: Uri;
    protected abstract _styleUris?: Uri[];
    protected abstract _specUri?: Uri;

    //#region [rgba(255, 255, 0, 0.07)] Public instance properties

    public get templateUri(): Uri | undefined {
      return this._templateUri || undefined;
    }

    public get styleUris(): Uri[] | undefined {
      return this._styleUris || [];
    }

    public get specUri(): Uri | undefined {
      return this._specUri || undefined;
    }

    //#endregion

    protected static _readFiles(
      scriptUri: Uri,
      nameFromFile: string,
      scriptDir: Uri
    ): Promise<INgFileData> {
      return this._readDecorator(scriptUri)
        .then(result => this._getFilesFromDecorator(
          nameFromFile,
          scriptDir,
          result?.decorator || "",
          result?.decoratorContent || "",
          result?.className || ""
        ))
        .then(([decorator, template, styles, spec, decoratorContent, className]) => {
          return {
            decorator: decorator,
            decoratorContent: decoratorContent,
            className: className,
            templateUri: template || undefined,
            styleUris: styles || [],
            specUri: spec || undefined
          };
        });
    }

    private static _readDecorator(script: Uri): Promise<INgFileData | null> {
      return Util.fromThenable(workspace.openTextDocument(script))
        .then(doc => {
          const decoratorRegex = /@(Injectable|NgModule|Component|Directive|Pipe|Guard)\s*?\([\s\n]*([^]*?)[\s\n]*?\)\s*?(@\w+|@\w+\s*?\([\s\n]*([^]*?)[\s\n]*?\))*\s*?(export)*\s*?class\s*?(\w+)/;
          const matchDecorator = doc.getText().match(decoratorRegex);
          return matchDecorator ? {
            decorator: matchDecorator[1],
            decoratorContent: matchDecorator[2],
            className: matchDecorator[6]
          } : null;
        });

    }

    private static _getFilesFromDecorator(
      nameFromFile: string,
      scriptDir: Uri,
      decorator: string,
      decoratorContent: string,
      className: string
    ) {
      const type = NgObjectType.getByIdentifier(decorator);
      if (!type) return Promise.reject(`Uri has no valid identifier`);
      const template = (decoratorContent && type.has(NgFileType.template)) ?
        this._getTemplateFile(decoratorContent, scriptDir) : null;
      const styles = (decoratorContent && type.has(NgFileType.style)) ?
        this._getStyleFiles(decoratorContent, scriptDir) : [];
      const spec = type.has(NgFileType.spec) ?
        this.getSpecFile(nameFromFile, scriptDir) :
        Promise.resolve(null);
      return Promise.all([decorator, template, styles, spec, decoratorContent, className]);
    }

    private static _getTemplateFile(content: string, script: Uri): Uri | null {
      const matchTemplateUrl = content.match(/templateUrl:\s*?['"](.*?)['"]/);
      if (!matchTemplateUrl) return null;
      const templateUrl = matchTemplateUrl[1];
      return templateUrl ? Uri.parse(Util.getAbsolutePath(script.toString(), templateUrl)) : null;
    }

    private static _getStyleFiles(content: string, script: Uri): Uri[] {
      const matchStyleList = content.match(/styleUrls:\s*?\[([^]*?)\]/);
      if (!matchStyleList) return [];
      return matchStyleList[1].replace(/['"]/g, '')
        .split(',')
        .map(style => Uri.parse(Util.getAbsolutePath(script.toString(), style.trim())));
    }

    private static getSpecFile(name: string, scriptDir: Uri): Promise<Uri | null> {
      const nameFromFile = `${name}.spec.ts`;
      return Util.fromThenable(workspace.openTextDocument(Uri.parse(scriptDir.toString() + '/' + nameFromFile)))
        .then(doc => doc.uri)
        .catch(() => Util.findFile(nameFromFile))
        .catch(() => null);
    }
}