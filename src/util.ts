import { workspace as ws, Uri, RelativePattern } from 'vscode';

export class Util {

  private constructor() { }

  public static findNgConfigFile(): Promise<Uri | null> {
    return this.fromThenable(ws.findFiles('angular.json', '**​/node_modules/**'))
      .then(uris => {
        if (!uris || uris.length < 1) return null;
        return uris[0];
      });
  }

  public static fromThenable<T>(thenable: Thenable<T>): Promise<T> {
    return new Promise((resolve, reject) => {
      thenable.then(
        r => resolve(r),
        e => reject(e)
      );
    });
  }

  public static findFile(glob: string): Promise<Uri> {
    if (!ws.workspaceFolders) return Promise.reject("No workspace.");
    return this._getFiles(ws.workspaceFolders[0].uri.fsPath, "", glob)
      .then(result => result[0]);
  }

  public static findFiles(glob: string, projectRoot: string): Promise<Uri[]> {
    if(!ws.workspaceFolders) return Promise.reject("No workspace.");
    return this._getFiles(ws.workspaceFolders[0].uri.fsPath, projectRoot, glob);
  }

  private static _getFiles(fs: string, path: string, glob: string) {
    const pattern = new RelativePattern(`${fs}/${path}`, `**${glob}`);
    const nodeModule = new RelativePattern(`${fs}/${path}`, '**​/node_modules/**');
    return this.fromThenable(ws.findFiles(pattern, nodeModule)).catch((): Uri[] => []);
  }

  public static getAbsolutePath(base: string, relative: string): string {
    const stack = base.split("/");
    relative.split("/").forEach(part => {
      if (part === ".") return;
      else if (part === "..") stack.pop();
      else stack.push(part);
    });
    return stack.join("/");
  }
}