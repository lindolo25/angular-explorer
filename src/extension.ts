import { Disposable, Uri, ViewColumn, commands, window } from 'vscode';
import { AngularDataProvider } from './tree/angularDataProvider';
import { NgTreeItem } from './tree/ngTreeItem';

const disposables: Disposable[] = [];
const openCommand = 'angularExplorer.openNgObject';
const refreshCommand = 'angularExplorer.refreshExplorer';

export function activate() {
  const provider = new AngularDataProvider();
  disposables.push(
    window.registerTreeDataProvider('angularExplorer', provider),
    commands.registerCommand(openCommand, (i: NgTreeItem) => _open(i.resourceUri)),
    commands.registerCommand(refreshCommand, () => provider.refresh())
  );
}

function _open(uri: Uri | undefined, preview: boolean = true): void {
  if (!uri) return;
  window.showTextDocument(uri, { preview: preview, viewColumn: ViewColumn.Active })
    .then(
      () => { return; },
      reason => { window.showErrorMessage(reason); }
    );
}

export function deactivate() {
  disposables.forEach(d => d.dispose());
}